import React from 'react';
import styled from 'styled-components';
import tokens from '../../tokens';
import Lottie from 'react-lottie';

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var colorsMap = {
  primary: {
    backgroundColor: tokens.colorBrandPrimaryPure,
    backgroundColorHover: tokens.colorBrandPrimaryDark,
    color: tokens.colorBackground04
  },
  orange: {
    backgroundColor: tokens.colorBrandSecondaryPure,
    backgroundColorHover: tokens.colorBrandSecondayDark,
    color: tokens.colorBackground04
  }
};
var Base = styled.button.withConfig({
  displayName: "styles__Base",
  componentId: "sc-15bmy8c-0"
})(["border:none;padding:16px;color:", ";background-color:", ";&&[disabled]{cursor:not-allowed;opacity:0.8;background-color:", ";border-color:", ";color:", ";}:hover{background-color:", ";}"], function (props) {
  return colorsMap[props.color].color;
}, function (props) {
  return colorsMap[props.color].backgroundColor;
}, tokens.colorBackground03, tokens.colorNeutralLight, tokens.colorNeutralMedium, function (props) {
  return colorsMap[props.color].backgroundColorHover;
});

var Button = React.forwardRef(function (_ref, ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      _ref$onClick = _ref.onClick,
      onClick = _ref$onClick === void 0 ? function () {} : _ref$onClick,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? "primary" : _ref$color,
      props = _objectWithoutPropertiesLoose(_ref, ["children", "onClick", "color"]);

  var handleClick = function handleClick(event) {
    onClick(event);
  };

  return /*#__PURE__*/React.createElement(Base, _extends({
    ref: ref,
    onClick: handleClick,
    color: color
  }, props), children);
});
Button.displayName = "Button";

var Animation = function Animation(_ref) {
  var _ref$autoplay = _ref.autoplay,
      autoplay = _ref$autoplay === void 0 ? true : _ref$autoplay,
      height = _ref.height,
      width = _ref.width,
      fileAnimation = _ref.fileAnimation,
      _ref$loop = _ref.loop,
      loop = _ref$loop === void 0 ? true : _ref$loop,
      _ref$onCompleteAnimat = _ref.onCompleteAnimation,
      onCompleteAnimation = _ref$onCompleteAnimat === void 0 ? function () {} : _ref$onCompleteAnimat,
      _ref$preserveAspectRa = _ref.preserveAspectRatio,
      preserveAspectRatio = _ref$preserveAspectRa === void 0 ? "xMidYMid slice" : _ref$preserveAspectRa;
  var defaultOptions = {
    loop: loop,
    autoplay: autoplay,
    renderer: "svg",
    animationData: fileAnimation,
    rendererSettings: {
      preserveAspectRatio: preserveAspectRatio
    }
  };
  return /*#__PURE__*/React.createElement(Lottie, {
    options: defaultOptions,
    height: height,
    width: width,
    eventListeners: [{
      eventName: "complete",
      callback: function callback() {
        return onCompleteAnimation;
      }
    }],
    isClickToPauseDisabled: true
  });
};

var OPTIONAL_FIELD_WIDTH = 87; // 55px base from the text + 12px (horizontal padding) * 2

var paddingRightMap = {
  none: 0,
  optionalField: OPTIONAL_FIELD_WIDTH - 6,
  // the full value pushes the text too much to the right
  errorIcon: 48
};
var Container = styled.div.withConfig({
  displayName: "styles__Container",
  componentId: "tqwr8c-0"
})(["position:relative;display:inline-flex;width:100%;"]);
var Input = styled.input.withConfig({
  displayName: "styles__Input",
  componentId: "tqwr8c-1"
})(["-webkit-appearance:none;-moz-appearance:none;appearance:none;width:100%;height:48px;padding:10px 24px;box-sizing:border-box;transition:border 0.2s ease;color:", ";font-size:inherit;font-family:inherit;outline:0;border:solid 1px ", ";border-radius:1px;padding-right:", ";[data-whatintent=\"mouse\"] &:hover,&:focus{border-color:", ";}&::-ms-clear{display:none;}&::placeholder{color:", ";}&&[disabled]{cursor:not-allowed;opacity:0.8;background-color:", ";border-color:", ";color:", ";}"], tokens.colorNeutralXdark, tokens.colorNeutralLight, function (props) {
  return paddingRightMap[props.rightElementType] + "px";
}, tokens.colorNeutralMedium, tokens.colorNeutralMedium, tokens.colorBackground03, tokens.colorNeutralLight, tokens.colorNeutralMedium);

var Input$1 = React.forwardRef(function (_ref, ref) {
  var isNumeric = _ref.isNumeric,
      className = _ref.className,
      isDisabled = _ref.isDisabled,
      props = _objectWithoutPropertiesLoose(_ref, ["isNumeric", "className", "isDisabled"]);

  var rightElementType = "none";
  var numericProps = isNumeric ? {
    inputMode: "numeric",
    pattern: "[0-9]*"
  } : {};
  return /*#__PURE__*/React.createElement(Container, {
    className: className
  }, /*#__PURE__*/React.createElement(Input, _extends({
    disabled: isDisabled,
    rightElementType: rightElementType,
    ref: ref
  }, numericProps, props)));
});
Input$1.displayName = "Input";

var Base$1 = styled.section.withConfig({
  displayName: "styles__Base",
  componentId: "y0nivf-0"
})(["margin-bottom:40px;"]);

var Section = function Section(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      props = _objectWithoutPropertiesLoose(_ref, ["children"]);

  return /*#__PURE__*/React.createElement(Base$1, props, children);
};
Section.displayName = "Section";

var Base$2 = styled.h2.withConfig({
  displayName: "styles__Base",
  componentId: "sc-1eii8su-0"
})(["color:", ";"], tokens.colorNeutralDark);

var TitleSection = function TitleSection(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      props = _objectWithoutPropertiesLoose(_ref, ["children"]);

  return /*#__PURE__*/React.createElement(Base$2, props, children);
};
TitleSection.displayName = "TitleSection";

var Base$3 = styled.h3.withConfig({
  displayName: "styles__Base",
  componentId: "sc-1q1jiwk-0"
})(["color:", ";"], tokens.colorNeutralDark);

var SubTitleSection = function SubTitleSection(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      props = _objectWithoutPropertiesLoose(_ref, ["children"]);

  return /*#__PURE__*/React.createElement(Base$3, props, children);
};
SubTitleSection.displayName = "SubTitleSection";

var Base$4 = styled.div.withConfig({
  displayName: "styles__Base",
  componentId: "sc-14uofre-0"
})(["border:solid 2px ", ";padding:10px;box-shadow:2px 2px ", ";"], tokens.colorBrandPrimaryLight, tokens.colorBrandPrimaryLight);

var Card = function Card(_ref) {
  var _ref$children = _ref.children,
      children = _ref$children === void 0 ? null : _ref$children,
      props = _objectWithoutPropertiesLoose(_ref, ["children"]);

  return /*#__PURE__*/React.createElement(Base$4, props, children);
};
Card.displayName = "Card";

export { Animation, Button, Card, Input$1 as Input, Section, SubTitleSection, TitleSection };
