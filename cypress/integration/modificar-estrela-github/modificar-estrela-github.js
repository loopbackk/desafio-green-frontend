import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("Eu abro a home page", () => {
  cy.openHome();
});

When(`Eu digito na barra de busca {string}`, (name) => {
  cy.get("[data-test-id='searchUserInput']").type(name);
});

When(`Eu clico em Buscar`, () => {
  cy.get("[data-test-id='searchUserButton']").click();
});

When(`Sou autenticado por meio do token`, () => {
  cy.doLogin();
  cy.reload();
});

When(`Vejo que existem repositórios no qual dei estrelas`, () => {
  cy.get("[data-test-id='RepositoryFillStar']").should("have.length.gte", 0);
});

When(`Clico na estrela do primeiro repositório com estrela`, () => {
  let count = 0;

  cy.get("[data-test-id='RepositoryFillStar']").each(() => {
    count++;
  });

  cy.get("[data-test-id='RepositoryFillStar']").first().click();

  Then(`Deveria remover a estrela do repositório da lista`, () => {
    cy.get("[data-test-id='RepositoryFillStar']").should("have.length.lt", count);
  });

  When(`Clico na estrela removida anteriormente`, () => {
    cy.get("[data-test-id='RepositoryOutlineStar']").first().click();
  });

  Then(`Deveria adicionar a estrela do repositório da lista`, () => {
    cy.get("[data-test-id='RepositoryFillStar']").should("have.length", count);
  });
});
