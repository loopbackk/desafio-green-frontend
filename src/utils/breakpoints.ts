export const breakpoints = {
  extraSmall: "361px",
  small: "641px",
  medium: "997px",
  large: "1281px",
};

export const queries = {
  forMobileOnly: `(max-width: 640px)`,
  forSmallMobileUp: `(min-width: ${breakpoints.extraSmall})`,
  forTabletUp: `(min-width: ${breakpoints.small})`,
  forDesktopUp: `(min-width: ${breakpoints.medium})`,
  forLargeDesktopUp: `(min-width: ${breakpoints.large})`,
};

export const media = {
  forMobileOnly: `@media ${queries.forMobileOnly}`,
  forSmallMobileUp: `@media ${queries.forSmallMobileUp}`,
  forTabletUp: `@media ${queries.forTabletUp}`,
  forDesktopUp: `@media ${queries.forDesktopUp}`,
  forLargeDesktopUp: `@media ${queries.forLargeDesktopUp}`,
};
