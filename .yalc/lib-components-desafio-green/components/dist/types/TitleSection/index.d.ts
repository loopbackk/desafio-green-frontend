export declare const TitleSection: {
    ({ children, ...props }: {
        [x: string]: any;
        children?: any;
    }): JSX.Element;
    displayName: string;
};
