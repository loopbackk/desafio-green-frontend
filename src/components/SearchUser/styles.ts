import styled from "styled-components";
import { Button as ButtonBase } from "lib-components-desafio-green/components";
import { media } from "../../utils/breakpoints";

export const Base = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const TitleSearch = styled.h1`
  text-align: center;
  font-size: 18px;
  ${media.forTabletUp} {
    font-size: 25px;
  }
`;

export const Button = styled(ButtonBase)`
  margin-top: 16px;
  ${media.forTabletUp} {
    margin-top: 0;
  }
`;

export const ContainerButtonSearch = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  ${media.forTabletUp} {
    flex-direction: row;
  }
`;
