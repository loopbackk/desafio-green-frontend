import { ReactNode } from "react";
declare type AnimationProps = {
    height: string;
    width: string;
    fileAnimation: ReactNode;
    loop?: boolean;
    autoplay?: boolean;
    onCompleteAnimation?: () => void;
    preserveAspectRatio?: string;
};
export declare const Animation: ({ autoplay, height, width, fileAnimation, loop, onCompleteAnimation, preserveAspectRatio, }: AnimationProps) => JSX.Element;
export {};
