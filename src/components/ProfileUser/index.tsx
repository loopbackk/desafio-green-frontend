import React from "react";
import * as Styled from "./styles";

export type ProfileUserProps = {
  avatar: string;
  name: string;
  url: string;
  bio: string;
};

export const ProfileUser = ({ name, avatar, url, bio, ...props }: ProfileUserProps) => {
  return (
    <Styled.Base {...props}>
      <Styled.ContainerTop>
        <Styled.ImagePerfil src={avatar} />
        <Styled.ContainerNames>
          <Styled.Username>{name}</Styled.Username>
          <Styled.UrlUser href={url} target={"_blank"}>
            {url}
          </Styled.UrlUser>
          <Styled.Bio>{bio}</Styled.Bio>
        </Styled.ContainerNames>
      </Styled.ContainerTop>
    </Styled.Base>
  );
};

ProfileUser.displayName = "ProfileUser";
