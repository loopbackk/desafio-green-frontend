import React, { useCallback, useEffect, useState } from "react";
import {
  callbackLoginGithub,
  doLoginGithub,
  getProfileOwnerGithub,
} from "../../services/github";
import { Profile } from "../../models";

const TOKEN_PROP = "token";

type TypeContext = {
  loading: boolean;
  token?: string | null;
  loginCallback: (token: string) => void;
  doLogin: () => void;
  doLogout: () => void;
  isError: boolean;
  user: Profile | null | undefined;
};

const AuthContext = React.createContext<TypeContext>({
  loading: false,
  token: null,
  loginCallback: () => {},
  doLogin: () => {},
  doLogout: () => {},
  isError: false,
  user: null,
} as TypeContext);

type PropsContext = {
  children: React.ReactNode;
};

export const AuthContextContextProvider = ({ children }: PropsContext) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [token, setToken] = useState<string | null>();
  const [user, setUser] = useState<Profile | null>(null);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    if (localStorage.getItem(TOKEN_PROP)) {
      setToken(localStorage.getItem(TOKEN_PROP));
    }
  }, []);

  useEffect(() => {
    if (!token) return;

    getProfileOwnerGithub().then((data: Profile) => {
      setUser(data);
    });
  }, [token]);

  const doLogin = () => {
    if (localStorage.getItem(TOKEN_PROP)) return;
    doLoginGithub();
  };

  const doLogout = useCallback(() => {
    localStorage.removeItem(TOKEN_PROP);
    setToken(null);
    setUser(null);
  }, [setToken]);

  const loginCallback = useCallback(
    (code: string) => {
      setIsError(false);
      setLoading(true);
      callbackLoginGithub(code)
        .then((data) => {
          console.log(data);
          if (data.data.access_token) {
            localStorage.setItem(TOKEN_PROP, data.data.access_token);
            setToken(data.data.access_token);
          } else {
            setIsError(true);
          }
        })
        .catch((error) => {
          console.log(error);
          setIsError(true);
        })
        .finally(() => {
          setTimeout(() => {
            setLoading(false);
          }, 2000);
        });
    },
    [setLoading]
  );

  const value = React.useMemo(
    () => ({
      loading,
      token,
      loginCallback,
      isError,
      doLogin,
      doLogout,
      user,
    }),
    [loading, loginCallback, token, isError, doLogout, user]
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthContext;
