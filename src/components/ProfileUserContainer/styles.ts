import styled from "styled-components";
import tokens from "lib-components-desafio-green/tokens";

export const Base = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const ImagePerfil = styled.img`
  max-height: 150px;
  max-width: 150px;
  border-radius: 50%;
`;

export const ContainerTop = styled.div`
  display: flex;
  justify-content: start;
`;

export const ContainerNames = styled.div`
  display: flex;
  justify-content: start;
  flex-direction: column;
  margin-left: 16px;
`;

export const UrlUser = styled.a`
  line-height: 24px;
  letter-spacing: 0;
  color: ${tokens.colorBrandPrimaryPure};
  text-decoration: none;
  cursor: pointer;
`;

export const Username = styled.p`
  font-size: 20px;
  font-weight: 700;
  margin-bottom: 10px;
`;

export const Bio = styled.p`
  color: ${tokens.colorNeutralMedium};
`;

export const LoadingContainer = styled.div`
  margin: 20px;
`;
