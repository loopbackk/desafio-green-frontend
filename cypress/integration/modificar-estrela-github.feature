Feature: Modificar Estrelas

  Eu quero remover ou adicionar uma estrela em um repositório github

  @focus
  Scenario: Remover estrela de um repositório github
    Given Eu abro a home page
    When Sou autenticado por meio do token
    When Eu digito na barra de busca "LuanPereiraLima"
    When Eu clico em Buscar
    When Vejo que existem repositórios no qual dei estrelas
    When Clico na estrela do primeiro repositório com estrela
    Then Deveria remover a estrela do repositório da lista

  Scenario: Adicionar estrela de um repositório github
    Given Eu abro a home page
    When Sou autenticado por meio do token
    When Eu digito na barra de busca "LuanPereiraLima"
    When Eu clico em Buscar
    When Vejo que existem repositórios no qual dei estrelas
    When Clico na estrela do primeiro repositório com estrela
    Then Deveria remover a estrela do repositório da lista
    When Clico na estrela removida anteriormente
    Then Deveria adicionar a estrela do repositório da lista
