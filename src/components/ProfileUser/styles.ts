import styled from "styled-components";
import tokens from "lib-components-desafio-green/tokens";
import { media } from "../../utils/breakpoints";

export const Base = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const ImagePerfil = styled.img`
  max-height: 150px;
  max-width: 150px;
  border-radius: 50%;
`;

export const ContainerTop = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  ${media.forTabletUp} {
    flex-direction: row;
    justify-content: start;
    text-align: left;
  }
`;

export const ContainerNames = styled.div`
  display: flex;
  justify-content: start;
  flex-direction: column;
  margin-left: 16px;
`;

export const UrlUser = styled.a`
  line-height: 24px;
  letter-spacing: 0;
  color: ${tokens.colorBrandPrimaryPure};
  text-decoration: none;
  cursor: pointer;
`;

export const Username = styled.p`
  font-size: 20px;
  font-weight: 700;
  margin-bottom: 10px;
`;

export const Bio = styled.p`
  color: ${tokens.colorNeutralMedium};
`;

export const LoadingContainer = styled.div`
  margin: 20px;
`;
