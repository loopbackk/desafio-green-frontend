import React, { ReactNode } from "react";
import { ProfileContextProvider } from "./contexts/profile";
import { RepositoryContextProvider } from "./contexts/repository";
import { AuthContextContextProvider } from "./contexts/authentication";
import { GeneralContextProvider } from "./contexts/general";

type MainContextsProps = {
  children: ReactNode;
};

const MainContexts = ({ children }: MainContextsProps) => {
  return (
    <GeneralContextProvider>
      <AuthContextContextProvider>
        <RepositoryContextProvider>
          <ProfileContextProvider>{children}</ProfileContextProvider>
        </RepositoryContextProvider>
      </AuthContextContextProvider>
    </GeneralContextProvider>
  );
};

export default MainContexts;
