# Introduction

`lib-components-desafio-green` is a library built to help create accessible digital products

# Development guide

## Setup

Install all the dependencies with `yarn install`.

## Building

To completely build the lib, run the `yarn prepare` command.

## Testing

- To run all the tests, run `yarn test`.

## Locally testing the library

To test the library with a local project (target project), we recommend using the library [yalc](https://www.npmjs.com/package/yalc): you can install it globally with `npm i -g yalc`.

After doing the changes you want, publish (this is a local publish) the library with `yalc publish` and install it in the target project with `yalc add lib-components-desafio-green`.

