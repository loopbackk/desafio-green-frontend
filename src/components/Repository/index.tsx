import React from "react";
import * as Styled from "./styles";
import { AiFillLock, AiFillUnlock, AiFillStar, AiOutlineStar } from "react-icons/ai";

export type RepositoryProps = {
  name: string;
  description: string;
  url: string;
  isPrivate: boolean;
  onClickStar: (typeAction: "REMOVE" | "ADD") => void;
  isStared: boolean | undefined;
};

export const Repository = ({
  name,
  description,
  url,
  isPrivate,
  onClickStar = () => {},
  isStared,
  ...props
}: RepositoryProps) => {
  const RenderStar = () => {
    if (isStared === undefined) {
      return null;
    } else if (isStared) {
      return (
        <AiFillStar
          data-test-id="RepositoryFillStar"
          color={"green"}
          size={20}
          onClick={() => onClickStar("REMOVE")}
        />
      );
    }
    return (
      <AiOutlineStar
        data-test-id="RepositoryOutlineStar"
        color={"green"}
        size={20}
        onClick={() => onClickStar("ADD")}
      />
    );
  };

  return (
    <Styled.Base {...props}>
      <Styled.TitleContainer>
        {isPrivate ? <AiFillLock /> : <AiFillUnlock />}
        <Styled.Title>{name}</Styled.Title>
        <Styled.Stars>{RenderStar()}</Styled.Stars>
      </Styled.TitleContainer>
      <Styled.DescriptionContainer>
        <Styled.Description>{description}</Styled.Description>
        <Styled.Url href={url} target="_blank">
          {url}
        </Styled.Url>
      </Styled.DescriptionContainer>
    </Styled.Base>
  );
};

Repository.displayName = "Repository";
