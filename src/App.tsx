// eslint-disable-next-line @typescript-eslint/no-var-requires
const MediaQuery = require("react-media-query-hoc");
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { queries } from "./utils/breakpoints";
import Routes from "./routes";
import MainContexts from "./mainContexts";
import { Loading } from "./components";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <MediaQuery.MediaQueryProvider queries={queries}>
        <React.Suspense fallback={<Loading fullScreen />}>
          <MainContexts>
            <Routes />
          </MainContexts>
        </React.Suspense>
      </MediaQuery.MediaQueryProvider>
    </BrowserRouter>
  );
};

export default App;
