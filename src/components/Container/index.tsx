import React, { ReactNode } from "react";
import * as Styles from "./styles";

type WrapperProps = {
  extraSmall?: string;
  small?: string;
  medium?: string;
  large?: string;
  children: ReactNode;
};

export const Wrapper = ({
  extraSmall = "321px",
  small = "440px",
  medium = "944px",
  large = "1032px",
  children = null,
  ...rest
}: WrapperProps) => {
  return (
    <Styles.Container
      small={small}
      medium={medium}
      large={large}
      extraSmall={extraSmall}
      {...rest}
    >
      {children}
    </Styles.Container>
  );
};
