import axios from "axios";

const TOKEN_PROP = "token";

const getToken = () => {
  return localStorage.getItem(TOKEN_PROP) || "";
};

export const axiosInstance = (defaultTokenParam = "", baseURL = "") => {
  let defaultToken = defaultTokenParam;

  if (!defaultToken) defaultToken = getToken();

  if (defaultToken !== "")
    axios.defaults.headers.common = {
      Authorization: `Bearer ${defaultToken}`,
    };

  return axios.create({
    baseURL,
  });
};
