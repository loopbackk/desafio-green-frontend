export declare const Container: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Input: import("styled-components").StyledComponent<"input", any, {
    rightElementType: string;
}, never>;
