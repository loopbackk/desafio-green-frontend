import React, { useState } from "react";

type Dispatch<A> = (value: A) => void;

type TypeContext = {
  loading: boolean;
  setLoading: Dispatch<boolean>;
};

const GeneralContext = React.createContext<TypeContext>({
  loading: false,
  setLoading: () => {},
} as TypeContext);

type PropsContext = {
  children: React.ReactNode;
};

export const GeneralContextProvider = ({ children }: PropsContext) => {
  const [loading, setLoading] = useState<boolean>(false);

  const value = React.useMemo(
    () => ({
      loading,
      setLoading,
    }),
    [loading, setLoading]
  );

  return <GeneralContext.Provider value={value}>{children}</GeneralContext.Provider>;
};

export default GeneralContext;
