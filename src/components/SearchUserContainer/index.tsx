import React, { useContext } from "react";
import { SearchUser } from "../SearchUser";
import AuthContext from "../../contexts/authentication";
import profileContext from "../../contexts/profile";
import repositoryContext from "../../contexts/repository";

export const SearchUserContainer = () => {
  const { user } = useContext(AuthContext);
  const { getProfile } = useContext(profileContext);
  const { getRepositories } = useContext(repositoryContext);

  const handleSearchUser = (username: string) => {
    getProfile(username);
    getRepositories(username);
  };

  return (
    <SearchUser
      userNameLog={user && user.name ? user.name : null}
      onClickSearch={handleSearchUser}
    />
  );
};

SearchUserContainer.displayName = "SearchUserContainer";
