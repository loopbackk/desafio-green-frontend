import * as PacmanLoading from "./animations/pacman-loading.json";
import * as GithubAnimation from "./animations/github-logo-octocat-animated.json";
import * as ProfileSkeleton from "./animations/profile-skeleton.json";
import * as SimpleProfile from "./animations/profile.json";
import * as Rocket from "./animations/rocket.json";
import * as MapLoading from "./animations/map-laoding.json";

export {
  PacmanLoading,
  GithubAnimation,
  ProfileSkeleton,
  SimpleProfile,
  Rocket,
  MapLoading,
};
