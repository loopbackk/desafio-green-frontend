import React, { useCallback, useContext, useEffect, useState } from "react";
import { Repository } from "../../models";
import {
  getRepositoriesStarsGithub,
  starRepositoryGithub,
  unStarRepositoryGithub,
} from "../../services/github";
import AuthContext from "../authentication";
import GeneralContext from "../general";

type TypeContext = {
  loading: boolean;
  repositories?: Repository[] | [];
  getRepositories: (username: string) => void;
  setStarRepository: (
    ownerName: string,
    repositoryName: string,
    typeAction: "ADD" | "REMOVE"
  ) => void;
};

const RepositoryContext = React.createContext<TypeContext>({
  loading: false,
  repositories: [],
  getRepositories: () => {},
  setStarRepository: () => {},
} as TypeContext);

type PropsContext = {
  children: React.ReactNode;
};

export const RepositoryContextProvider = ({ children }: PropsContext) => {
  const { user } = useContext(AuthContext);
  const { setLoading: setLoadingGeneral } = useContext(GeneralContext);

  const [loading, setLoading] = useState<boolean>(false);
  const [repositories, setRepositories] = useState<Repository[]>([]);

  const getRepositories = useCallback(
    (username: string) => {
      setLoading(true);
      getRepositoriesStarsGithub(username)
        .then((repositories: Repository[]) => {
          setRepositories(repositories);
        })
        .catch((error) => {
          console.log(error);
          setRepositories([]);
        })
        .finally(() => {
          setTimeout(() => {
            setLoading(false);
          }, 2000);
        });
    },
    [setRepositories, setLoading]
  );

  const setStarRepository = (
    ownerName: string,
    repositoryName: string,
    typeAction: "ADD" | "REMOVE"
  ) => {
    setLoadingGeneral(true);
    if (typeAction === "ADD") {
      starRepositoryGithub(ownerName, repositoryName)
        .then(() => {
          setRepositories(
            repositories.map((repository) => {
              if (repository.name === repositoryName) {
                repository.ownerStar = true;
              }
              return repository;
            })
          );
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setLoadingGeneral(false);
        });
    } else if (typeAction === "REMOVE") {
      unStarRepositoryGithub(ownerName, repositoryName)
        .then(() => {
          setRepositories(
            repositories.map((repository) => {
              if (repository.name === repositoryName) {
                repository.ownerStar = false;
              }
              return repository;
            })
          );
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setLoadingGeneral(false);
        });
    } else {
      setLoadingGeneral(false);
    }
  };

  const isRepositoriesVerified = (repositoriesParam: Repository[]) => {
    return !(repositoriesParam[0].ownerStar === undefined);
  };

  useEffect(() => {
    if (
      user &&
      repositories &&
      repositories.length > 0 &&
      !isRepositoriesVerified(repositories)
    ) {
      getRepositoriesStarsGithub(user.login)
        .then((repositoriesOwner: Repository[]) => {
          const repositoriesVerified = repositories.map((repository) => {
            const found = repositoriesOwner.findIndex(
              (repositoryOwner) => repositoryOwner.id === repository.id
            );
            repository.ownerStar = found > -1;
            return repository;
          });
          setRepositories(repositoriesVerified);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setTimeout(() => {
            setLoading(false);
          }, 4000);
        });
    }
  }, [user, repositories]);

  const value = React.useMemo(
    () => ({
      loading,
      repositories,
      getRepositories,
      setStarRepository,
    }),
    [loading, getRepositories, repositories, setStarRepository]
  );

  return (
    <RepositoryContext.Provider value={value}>{children}</RepositoryContext.Provider>
  );
};

export default RepositoryContext;
