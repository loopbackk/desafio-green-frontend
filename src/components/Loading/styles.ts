import styled from "styled-components";

export const Base = styled.div<{ fullScreen: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: ${(props) => (props.fullScreen ? "100vh" : "100%")};
`;
