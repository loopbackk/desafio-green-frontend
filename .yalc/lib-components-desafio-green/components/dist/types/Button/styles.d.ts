import { ButtonColor } from ".";
declare type StyleButtonProps = {
    color: ButtonColor;
};
export declare const Base: import("styled-components").StyledComponent<"button", any, StyleButtonProps, never>;
export {};
