export interface Location {
  lat: number;
  lng: number;
  nameUser?: string;
}
