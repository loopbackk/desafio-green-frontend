type RoutesProps = {
  HOME: string;
  LOGIN_CALLBACK: string;
};

export const ROUTES = {
  HOME: "/home",
  LOGIN_CALLBACK: "/login-callback",
} as RoutesProps;

type UrlsProps = {
  BASE_API: string;
  URL_CALLBACK_GITHUB: string;
  GITHUB_URL_API: string;
  GITHUB_AUTHORIZE_URL: string;
};

export const URLS = {
  BASE_API: process.env.REACT_APP_BASE_URL_API,
  URL_CALLBACK_GITHUB: process.env.REACT_APP_URL_CALLBACK_GITHUB,
  GITHUB_URL_API: process.env.REACT_APP_GITHUB_API_URL,
  GITHUB_AUTHORIZE_URL: process.env.REACT_APP_GITHUB_AUTHORIZE_URL,
} as UrlsProps;

type KeysProps = {
  API_MAP: string;
  CLIENT_ID: string;
  CLIENT_SECRET: string;
};

export const KEYS = {
  API_MAP: process.env.REACT_APP_API_CODE_MAP,
  CLIENT_ID: process.env.REACT_APP_CLIENT_ID,
  CLIENT_SECRET: process.env.REACT_APP_CLIENT_SECRET,
} as KeysProps;

type GeneralProps = {
  SCOPES: string;
};

export const GENERAL = {
  SCOPES: process.env.REACT_APP_GITHUB_SCOPES,
} as GeneralProps;
