import styled from "styled-components";

import { breakpoints } from "../../utils/breakpoints";

const { extraSmall, small, medium, large } = breakpoints;

export const Container = styled.div<{
  extraSmall?: string;
  small?: string;
  medium?: string;
  large?: string;
}>`
  position: relative;
  width: auto;
  max-width: 296px;
  margin: 0 auto;

  @media (min-width: ${extraSmall}) {
    ${(props) => `max-width: ${props.extraSmall};`}
  }

  @media (min-width: ${small}) {
    ${(props) => `max-width: ${props.small};`}
  }

  @media (min-width: ${medium}) {
    ${(props) => `max-width: ${props.medium};`}
  }

  @media (min-width: ${large}) {
    ${(props) => `max-width: ${props.large};`}
  }
`;
