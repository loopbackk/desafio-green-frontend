import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("Eu abro a home page", () => {
  cy.openHome();
});

When(`Eu digito na barra de busca {string}`, (name) => {
  cy.get("[data-test-id='searchUserInput']").type(name);
});

When(`Eu clico em Buscar`, () => {
  cy.get("[data-test-id='searchUserButton']").click();
});

Then(`Eu vejo os carregamentos`, () => {
  cy.get("[data-test-id='ProfileUserContainerLoading']").should("be.visible");
  cy.get("[data-test-id='RepositoriesContainerLoading']").should("be.visible");
  cy.get("[data-test-id='MapContainerLoading']").should("be.visible");
});

Then(
  `Eu vejo o perfil do usuário, mapa para localização do usuário e lista de repositórios`,
  () => {
    cy.get("[data-test-id='MapContainerRepositories']").should("be.visible");
    cy.get("[data-test-id='ProfileUserContainerProfile']").should("be.visible");
    cy.get("[data-test-id='MapContainerContent']").should("be.visible");
  }
);

When(`Eu digito na barra de busca do mapa {string}`, (name) => {
  cy.get("[data-test-id='MapContainerInput']").type(name);
});

When(`Eu clico em Comparar na busca do mapa`, () => {
  cy.get("[data-test-id='MapContainerButton']").click();
});

Then(`Eu vejo o carregamento da medição da distancia do traçamento da rota`, () => {
  cy.get("[data-test-id='GeneralLoading']").should("be.visible");
});

Then(
  `Eu vejo dois pontos marcados no mapa com o primeiro usuário buscado e o segundo com a distância e rota criada`,
  () => {
    cy.get("[data-test-id='MapMarker']").should("have.length", 2);
    cy.get("[data-test-id='MapInfoBox']").should("be.visible");
  }
);
