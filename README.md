# [Front Desafio Green](https://desafio-green-front.web.app/)

## Features Desenvolvidas
- [x] Uma tela inicial com um campo para receber o nome ou apelido de um usuário no GitHub; 


- [x] Se o usuário inserido na tela inicial for um usuário válido (se ele existir), os seguintes dados deverão ser exibidos:
-  - [x] Apelido, avatar, biografia e URL do usuário;
-  - [x] Um mapa com um marcador na localização do usuário;
-  - [x] Uma lista com os repositórios aos quais o usuário deu estrela no GitHub.


- [x] Testes unitários
- [x] Na lista de repositórios, permitir que um usuário logado dê ou remova uma estrela para
  cada repositório do usuário buscado;
- [x] Tela onde colocando 2 usuário, mostra no mapa distância entre eles e o caminho;
- [x] Testes End-to-end.

## Guia de Desenvolvimento

### Setups

- versão do node: `12.18.1`
- versão do yarn: `1.22.10`

- Instale as dependências usando: `yarn install`.

### Building

- para buildar o projeto use o comando: `yarn build`.

### Testes End-to-end

***IMPORTANTE: PARA RODAR TESTES QUE CONTENHAM LOGIN POR MEIO DO CYPRESS, É NECESSÁRIO ADICIONAR 
UM TOKEN DO GITHUB NAS VARIÁVEIS AMBIENTE DO CYPRESS. VOCÊ PODE FAZER ISSO MODIFICANDO O COMANDO `cypress:open` e `cypress:run` 
DENTRO DO `package.json`. ISSO FOI NECESSÁRIO, POIS O GITHUB NÃO ACEITA QUE O PROCESSO DE AUTENTICAÇÃO OCORRA DURANTE O TESTE
AUTOMATIZADO, ENTÃO FOI PRECISO TRABALHAR DIRETAMENTE COM O TOKEN***

- Para os testes foram utilizadas as bibliotecas: `cypress e cypress-cucumber-preprocessor`. 
  Dessa forma é possível trabalhar com o `Cucumber`, analisar a pasta (`cypres/integration`). 
  Os cenários são criados nos arquivos `.feature` e desenvolvidas dentro das suas pastas correspondentes.
- Foram focados apenas os testes End-to-End neste projeto, pelo tempo corrido do desenvolvimento.
Testes unitários foram adicionados na `lib-components-desafio-green`.

- Para rodar todos os testes, primeiro inicie o projeto: `yarn start`.
- Depois inicie os testes: `yarn cypress:run`

- Caso queira ver a execução acontecendo, ou fazer debug dos cenários: `yarn cypress:open`

### Rodar localmente

- Execute o comando: `yarn start`.

### Firebase hosting

- O projeto foi configurado para ser disponibilizado por meio do firebase hosting,
  para isso é necessário executar o build (`yarn build`), e então disponibilizar
  no Google hosting (`firebase deploy`)
- O projeto ficará disponível no link: [Desafio Green](https://desafio-green-front.web.app/)


### Componentes compartilhados

- O projeto utiliza componentes compartilhados da `lib-components-desafio-green`, 
dessa forma é possível criar um ecossistema em que muitos outros projetos podem utilizar.
- Projeto disponível em [Repositório componentes](https://bitbucket.org/loopbackk/desafio-green-biblioteca-componentes)

### Backend Auxiliar

- Para a autenticação por meio do Github funcionar corretamente, foi necessário a criação de uma 
aplicação Backend para que ele fique responsável pela obtenção do token de autenticação do Github, 
  logo após o recebimento do `CODE` pelo Frontend.
- Projeto disponível em [Repositório back](https://bitbucket.org/loopbackk/desafio-green-backend)
  
### Estado global da aplicação

- O projeto utilizou-se apenas da `Context Api` disponível pela própria biblioteca `React`. Essa decisão foi 
tomada, pois, ela já entrega tudo que é necessário para o desenvolvimento do desafio.

![Alt text](./diagrama-estado.png?raw=true)

- Na imagem é possível avaliar como funciona o fluxo de informações na aplicação.