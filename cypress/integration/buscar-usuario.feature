Feature: Cenário Buscas

  Eu quero busca os usuários

  Scenario: Buscando usuário
    Given Eu abro a home page
    When Eu digito na barra de busca "LuanPereiraLima"
    When Eu clico em Buscar
    Then Eu vejo os carregamentos
    Then Eu vejo o perfil do usuário, mapa para localização do usuário e lista de repositórios

  Scenario: Buscando segundo usuário para medir distancia e traçar rota
    Given Eu abro a home page
    When Eu digito na barra de busca "mensonones"
    When Eu clico em Buscar
    Then Eu vejo os carregamentos
    Then Eu vejo o perfil do usuário, mapa para localização do usuário e lista de repositórios
    When Eu digito na barra de busca do mapa "LuanPereiraLima"
    When Eu clico em Comparar na busca do mapa
    Then Eu vejo o carregamento da medição da distancia do traçamento da rota
    Then Eu vejo dois pontos marcados no mapa com o primeiro usuário buscado e o segundo com a distância e rota criada