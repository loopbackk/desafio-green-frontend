import React, { useState } from "react";
import * as Styled from "./styles";
import { Input, Animation } from "lib-components-desafio-green/components";
import { GithubAnimation } from "../../assets";

export type SearchUserProps = {
  onClickSearch: (username: string) => void;
  userNameLog: string | null;
};

export const SearchUser = ({
  onClickSearch = () => {},
  userNameLog = null,
  ...props
}: SearchUserProps) => {
  const [username, setUsername] = useState<string>("");

  return (
    <Styled.Base {...props}>
      <Animation fileAnimation={GithubAnimation} height={"200px"} width={"200px"} />
      <Styled.TitleSearch data-test-id={"SearchUserTitleSearch"}>
        {userNameLog
          ? `${userNameLog.split(" ")[0]}, por favor, digite um usuário Github`
          : "Digite o usuário Github"}
      </Styled.TitleSearch>
      <Styled.ContainerButtonSearch>
        <Input
          data-test-id={"searchUserInput"}
          value={username}
          onChange={(event) => {
            setUsername(event.target.value);
          }}
        />
        <Styled.Button
          data-test-id={"searchUserButton"}
          onClick={() => onClickSearch(username)}
          disabled={!username}
        >
          Buscar
        </Styled.Button>
      </Styled.ContainerButtonSearch>
    </Styled.Base>
  );
};

SearchUser.displayName = "SearchUser";
