import React, { ReactNode, useEffect, useState } from "react";
import * as Styled from "./styles";
import {
  Marker,
  useJsApiLoader,
  GoogleMap,
  DirectionsRenderer,
  InfoBox,
} from "@react-google-maps/api";
import { KEYS } from "../../utils/constants";
import { Location } from "../../models";

export type MapProps = {
  markersCoords?: Location[] | null;
};

export const Map = ({ markersCoords, ...props }: MapProps) => {
  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: KEYS.API_MAP,
  });
  const [center, setCenter] = useState<Location>({ lat: 59.938043, lng: 30.337157 });
  const [markers, setMarkers] = useState<ReactNode[]>([]);
  const [directions, setDirections] = useState<google.maps.DirectionsResult>();
  const zoom = 9;

  useEffect(() => {
    if (!isLoaded || !(markersCoords && markersCoords.length > 0)) return;

    setCenter(markersCoords[0]);
    const markers = markersCoords.map((coord: Location) => {
      return (
        <div key={coord.lat} data-test-id={"MapMarker"}>
          <Marker
            position={{ lat: coord.lat, lng: coord.lng }}
            title={coord.nameUser}
            label={coord.nameUser}
          />
        </div>
      );
    });

    if (markersCoords.length > 1) {
      const directionsService = new google.maps.DirectionsService();
      const origin = new google.maps.LatLng(markersCoords[0].lat, markersCoords[0].lng);
      const destination = new google.maps.LatLng(
        markersCoords[1].lat,
        markersCoords[1].lng
      );
      directionsService.route(
        {
          origin,
          destination,
          travelMode: google.maps.TravelMode.DRIVING,
        },
        (result, status: string) => {
          if (status === google.maps.DirectionsStatus.OK) {
            console.log("Resut: ", result.routes[0].legs[0].distance);
            console.log("Resut: ", result.routes[0].legs[0].duration);
            setDirections(result);
          } else {
            console.error(`error fetching directions ${result}`);
          }
        }
      );
    }

    setMarkers(markers);
  }, [markersCoords, isLoaded]);

  if (!markers || !isLoaded) return null;

  return (
    <Styled.Base {...props}>
      <GoogleMap
        center={center}
        zoom={zoom}
        mapContainerStyle={{ width: "100%", height: "100%" }}
      >
        {markers}
        {directions && <DirectionsRenderer directions={directions} />}
        {directions && (
          <InfoBox
            position={
              new google.maps.LatLng(
                directions.routes[0].legs[0].start_location.lat(),
                directions.routes[0].legs[0].start_location.lng()
              )
            }
            options={{ closeBoxURL: ``, enableEventPropagation: true }}
          >
            <Styled.DistanceContainer data-test-id={"MapInfoBox"}>
              {directions.routes[0].legs[0].distance.text}
            </Styled.DistanceContainer>
          </InfoBox>
        )}
      </GoogleMap>
    </Styled.Base>
  );
};

Map.displayName = "Map";
