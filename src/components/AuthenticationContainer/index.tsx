import React, { useContext } from "react";
import * as Styled from "./styles";
import AuthContext from "../../contexts/authentication";
import { Button } from "lib-components-desafio-green/components";
import { AiOutlineLogout, AiOutlineLogin } from "react-icons/ai";

export const AuthenticationContainer = () => {
  const { token, doLogin, doLogout, user } = useContext(AuthContext);

  return (
    <Styled.Base>
      {user && (
        <Styled.Username data-test-id={"AuthenticationContainerUsername"}>
          {user.name}
        </Styled.Username>
      )}
      <Button
        data-test-id={"AuthenticationContainerButton"}
        color={"orange"}
        onClick={() => {
          if (token) {
            doLogout();
          } else {
            doLogin();
          }
        }}
      >
        {token ? (
          <Styled.ButtonName>
            <Styled.ButtonLabel>Sair</Styled.ButtonLabel> <AiOutlineLogout />
          </Styled.ButtonName>
        ) : (
          <Styled.ButtonName>
            <Styled.ButtonLabel>Entrar</Styled.ButtonLabel> <AiOutlineLogin />
          </Styled.ButtonName>
        )}
      </Button>
    </Styled.Base>
  );
};

AuthenticationContainer.displayName = "AuthenticationContainer";
