import styled from "styled-components";
import tokens from "lib-components-desafio-green/tokens";

export const Base = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: 0.5;
  background-color: ${tokens.colorBackground04};
`;
