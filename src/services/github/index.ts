import { axiosInstance } from "../servicesSetup";
import { KEYS, URLS, GENERAL } from "../../utils/constants";

export const getProfileGithub = async (user: string) => {
  const instance = await axiosInstance();
  const { data } = await instance.get(`${URLS.GITHUB_URL_API}users/${user}`);
  return data;
};

export const getProfileOwnerGithub = async () => {
  const instance = await axiosInstance();
  const { data } = await instance.get(`${URLS.GITHUB_URL_API}user`);
  return data;
};

export const getRepositoriesStarsGithub = async (user: string) => {
  const instance = await axiosInstance();
  const { data } = await instance.get(
    `${URLS.GITHUB_URL_API}users/${user}/starred?per_page=50`
  );
  return data;
};

export const doLoginGithub = () => {
  window.open(
    `${URLS.GITHUB_AUTHORIZE_URL}?client_id=${KEYS.CLIENT_ID}&scope=${GENERAL.SCOPES}&redirect_uri=${URLS.URL_CALLBACK_GITHUB}`,
    "_self"
  );
};

export const callbackLoginGithub = async (code: string) => {
  const instance = await axiosInstance();
  const { data } = await instance.post(`${URLS.BASE_API}access_token`, {
    code,
  });
  return data;
};

export const starRepositoryGithub = async (owner: string, repository: string) => {
  const instance = await axiosInstance();
  const { data } = await instance.put(
    `${URLS.GITHUB_URL_API}user/starred/${owner}/${repository}`
  );
  return data;
};

export const unStarRepositoryGithub = async (owner: string, repository: string) => {
  const instance = await axiosInstance();
  const { data } = await instance.delete(
    `${URLS.GITHUB_URL_API}user/starred/${owner}/${repository}`
  );
  return data;
};
