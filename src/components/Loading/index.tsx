import React from "react";
import * as Styled from "./styles";
import { Animation } from "lib-components-desafio-green/components";
import { PacmanLoading } from "../../assets";

export type LoadingProps = {
  fullScreen?: boolean;
  fileAnimation?: React.ReactNode;
  width?: string;
  height?: string;
};

export const Loading = ({
  fullScreen = false,
  fileAnimation = PacmanLoading,
  width = "200px",
  height = "200px",
  ...props
}: LoadingProps) => {
  return (
    <Styled.Base fullScreen={fullScreen} {...props}>
      <Animation width={width} height={height} fileAnimation={fileAnimation} />
    </Styled.Base>
  );
};

Loading.displayName = "Loading";
