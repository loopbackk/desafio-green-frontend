import React, { useContext, useEffect, useState } from "react";
import * as Styled from "./styles";
import ProfileContext from "../../contexts/profile";
import { Map } from "../Map";
import { Location } from "../../models";
import { Button, Input } from "lib-components-desafio-green/components";
import { Loading } from "../Loading";
import { MapLoading } from "../../assets";
import { TitleSection, SubTitleSection } from "lib-components-desafio-green/components";

export const MapContainer = () => {
  const { loading, location, getProfileToCompare, locationToCompare } = useContext(
    ProfileContext
  );
  const [markers, setMarkers] = useState<Location[]>();
  const [username, setUsername] = useState<string>("");

  useEffect(() => {
    if (location && locationToCompare) {
      setMarkers([location, locationToCompare]);
    } else if (location) {
      setMarkers([location]);
    }
  }, [location, locationToCompare]);

  if (loading)
    return <Loading data-test-id="MapContainerLoading" fileAnimation={MapLoading} />;

  if (!location) return null;

  if (markers)
    return (
      <React.Fragment>
        <TitleSection>Localização</TitleSection>
        <SubTitleSection>
          Digite um usuário para analisar distância e traçar caminho
        </SubTitleSection>
        <Styled.ContainerCompare>
          <Input
            data-test-id={"MapContainerInput"}
            value={username}
            onChange={(event) => {
              setUsername(event.target.value);
            }}
          />
          <Button
            data-test-id={"MapContainerButton"}
            onClick={() => getProfileToCompare(username)}
            disabled={!username}
          >
            Comparar
          </Button>
        </Styled.ContainerCompare>
        <Map markersCoords={markers} data-test-id={"MapContainerContent"} />
      </React.Fragment>
    );

  return null;
};

MapContainer.displayName = "MapContainer";
