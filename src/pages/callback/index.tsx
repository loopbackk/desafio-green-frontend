import React, { useContext, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import AuthContext from "../../contexts/authentication";
import { ROUTES } from "../../utils/constants";
import queryString from "query-string";
import { Rocket } from "../../assets";
import { Loading } from "../../components";

type Params = {
  code: string;
};

const LoginCallback = () => {
  const location = useLocation();
  const history = useHistory();
  const { loginCallback, loading, token, isError } = useContext(AuthContext);

  useEffect(() => {
    const params: Params = queryString.parse(location.search) as Params;

    if (!params || !params.code) history.replace(ROUTES.HOME);

    loginCallback(params.code);
  }, [location]);

  useEffect(() => {
    if (loading) return;
    if (isError) history.replace(ROUTES.HOME);
    if (token) history.replace(ROUTES.HOME);
  }, [loading, isError, token]);

  return (
    <Loading
      data-test-id="LoginCallbackLoading"
      fileAnimation={Rocket}
      fullScreen={true}
      width={"500px"}
      height={"500px"}
    />
  );
};

export default LoginCallback;
