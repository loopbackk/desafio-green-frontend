import React, { useContext } from "react";
import * as Styled from "./styles";
import ProfileContext from "../../contexts/profile";
import { SimpleProfile } from "../../assets";
import { Loading } from "../Loading";
import { ProfileUser } from "../ProfileUser";

export const ProfileUserContainer = ({ ...props }) => {
  const { loading, profile } = useContext(ProfileContext);

  if (loading)
    return (
      <Styled.LoadingContainer>
        <Loading
          data-test-id={"ProfileUserContainerLoading"}
          fileAnimation={SimpleProfile}
          fullScreen={false}
          height={"150px"}
          width={"150px"}
        />
      </Styled.LoadingContainer>
    );

  if (profile)
    return (
      <ProfileUser
        data-test-id={"ProfileUserContainerProfile"}
        url={profile.html_url}
        avatar={profile.avatar_url}
        bio={profile.bio}
        name={profile.name}
        {...props}
      />
    );

  return null;
};

ProfileUserContainer.displayName = "ProfileUserContainer";
