import React, { useCallback, useContext, useEffect, useState } from "react";
import { Profile } from "../../models";
import { getProfileGithub } from "../../services/github";
import Geocode from "react-geocode";
import { KEYS } from "../../utils/constants";
import { Location } from "../../models";
import GeneralContext from "../general";

type TypeContext = {
  loading: boolean;
  profile?: Profile | null;
  getProfile: (username: string) => void;
  location?: Location | null;
  profileToCompare?: Profile | null;
  getProfileToCompare: (username: string) => void;
  locationToCompare?: Location | null;
};

const ProfileContext = React.createContext<TypeContext>({
  loading: false,
  profile: null,
  getProfile: () => {},
  location: null,
  profileToCompare: null,
  getProfileToCompare: () => {},
  locationToCompare: null,
} as TypeContext);

type PropsContext = {
  children: React.ReactNode;
};

export const ProfileContextProvider = ({ children }: PropsContext) => {
  const { setLoading: setGeneralLoading } = useContext(GeneralContext);
  const [loading, setLoading] = useState<boolean>(false);
  const [profile, setProfile] = useState<Profile>();
  const [location, setLocation] = useState<Location>();
  const [profileToCompare, setProfileToCompare] = useState<Profile>();
  const [locationToCompare, setLocationToCompare] = useState<Location>();

  useEffect(() => {
    Geocode.setApiKey(KEYS.API_MAP);
    Geocode.setLanguage("en");
    Geocode.setRegion("es");
    Geocode.enableDebug();
  }, []);

  const getProfile = useCallback(
    (username: string) => {
      setLoading(true);
      getProfileGithub(username)
        .then((profile: Profile) => {
          setProfile(profile);
          setProfileToCompare(undefined);
          Geocode.fromAddress(profile.location)
            .then((item) => {
              setLocation({
                nameUser: profile.name,
                lat: item.results[0].geometry.location.lat,
                lng: item.results[0].geometry.location.lng,
              });
            })
            .catch((err) => {
              console.log("err: ", err);
              setLocation(undefined);
            });
        })
        .catch((error) => {
          console.log(error);
          setProfile(undefined);
          setProfileToCompare(undefined);
          setLocation(undefined);
          setLocationToCompare(undefined);
        })
        .finally(() => {
          setTimeout(() => {
            setLoading(false);
          }, 2000);
        });
    },
    [setProfile, setLoading]
  );

  const getProfileToCompare = useCallback(
    (username: string) => {
      setGeneralLoading(true);
      getProfileGithub(username)
        .then((profile: Profile) => {
          setProfileToCompare(profile);
          Geocode.fromAddress(profile.location)
            .then((item) => {
              setLocationToCompare({
                nameUser: profile.name,
                lat: item.results[0].geometry.location.lat,
                lng: item.results[0].geometry.location.lng,
              });
            })
            .catch((err) => {
              setProfileToCompare(undefined);
              console.log("err: ", err);
            });
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setTimeout(() => {
            setGeneralLoading(false);
          }, 2000);
        });
    },
    [setLoading, setProfileToCompare]
  );

  const value = React.useMemo(
    () => ({
      loading,
      profile,
      getProfile,
      location,
      getProfileToCompare,
      profileToCompare,
      locationToCompare,
    }),
    [
      loading,
      getProfile,
      profile,
      location,
      getProfileToCompare,
      profileToCompare,
      locationToCompare,
    ]
  );

  return <ProfileContext.Provider value={value}>{children}</ProfileContext.Provider>;
};

export default ProfileContext;
