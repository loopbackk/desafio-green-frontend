import React, { useContext } from "react";
import { Switch, Redirect } from "react-router-dom";
import { ROUTES } from "./utils/constants";
import { GuardProvider, GuardedRoute } from "react-router-guards";
import {
  GuardFunctionRouteProps,
  GuardToRoute,
  Next,
  BaseGuardProps,
} from "react-router-guards/dist/types";
import GeneralContext from "./contexts/general";
import { GeneralLoading } from "./components";

const AsyncHome = React.lazy(() => import("./pages/home"));
const AsyncAuthentication = React.lazy(() => import("./pages/callback"));

const InitRoutes = () => {
  const { loading } = useContext(GeneralContext);
  const requireLogin = async (
    to: GuardToRoute,
    from: GuardFunctionRouteProps | null,
    next: Next
  ) => {
    next();
  };

  return (
    <>
      <GuardProvider>
        <Switch>
          <GuardedRoute path={ROUTES.HOME} exact component={AsyncHome} />
          <GuardedRoute
            path={ROUTES.LOGIN_CALLBACK}
            exact
            component={AsyncAuthentication}
          />
          <GuardedRoute
            path="*"
            component={(props: BaseGuardProps) => (
              <Redirect to={ROUTES.HOME} {...props} />
            )}
            guards={[requireLogin]}
          />
        </Switch>
      </GuardProvider>
      {loading && <GeneralLoading />}
    </>
  );
};

export default InitRoutes;
