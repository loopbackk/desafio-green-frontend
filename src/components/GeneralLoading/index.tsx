import React from "react";
import * as Styles from "./styles";
import { Loading } from "../Loading";

export const GeneralLoading = () => {
  return (
    <Styles.Base>
      <Loading
        data-test-id={"GeneralLoading"}
        fullScreen
        height={"150px"}
        width={"150px"}
      />
    </Styles.Base>
  );
};
