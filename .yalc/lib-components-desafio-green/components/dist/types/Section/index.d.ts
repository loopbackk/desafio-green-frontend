export declare const Section: {
    ({ children, ...props }: {
        [x: string]: any;
        children?: any;
    }): JSX.Element;
    displayName: string;
};
