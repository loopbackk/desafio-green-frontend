import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("Eu abro a home page", () => {
  cy.openHome();
});

When(`Sou autenticado por meio do token`, () => {
  cy.doLogin();
  cy.reload();
});

Then(`Vejo o botão de Sair e o nome do usuário logado ao lado deste mesmo botão`, () => {
  cy.get("[data-test-id='AuthenticationContainerButton']").should("contain", "Sair");
  cy.get("[data-test-id='AuthenticationContainerUsername']").should("be.visible");
  cy.get("[data-test-id='SearchUserTitleSearch']").should("contain", "por favor");
});
