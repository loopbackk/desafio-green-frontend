import React from "react";
import {
  ProfileUserContainer,
  RepositoriesContainer,
  Wrapper,
  MapContainer,
  AuthenticationContainer,
  SearchUserContainer,
} from "../../components";
import { Section } from "lib-components-desafio-green/components";

const Home = () => {
  return (
    <Wrapper>
      <AuthenticationContainer />
      <Section>
        <SearchUserContainer />
      </Section>
      <Section>
        <ProfileUserContainer />
      </Section>
      <Section>
        <MapContainer />
      </Section>
      <Section>
        <RepositoriesContainer />
      </Section>
    </Wrapper>
  );
};

export default Home;
