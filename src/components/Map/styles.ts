import styled from "styled-components";

export const Base = styled.div`
  width: 100%;
  height: 500px;
`;

export const DistanceContainer = styled.div`
  font-size: 20px;
  font-weight: bold;
`;
