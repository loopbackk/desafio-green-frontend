import styled from "styled-components";
import tokens from "lib-components-desafio-green/tokens";

export const Base = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 10px;
  align-items: center;
`;

export const ButtonName = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ButtonLabel = styled.span`
  margin-right: 5px;
`;

export const Username = styled.h3`
  color: ${tokens.colorBrandPrimaryPure};
  margin-right: 16px;
`;
