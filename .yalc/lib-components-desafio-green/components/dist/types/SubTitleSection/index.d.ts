export declare const SubTitleSection: {
    ({ children, ...props }: {
        [x: string]: any;
        children?: any;
    }): JSX.Element;
    displayName: string;
};
