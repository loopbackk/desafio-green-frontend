import React, { useContext } from "react";
import * as Styled from "./styles";
import RepositoryContext from "../../contexts/repository";
import AuthContext from "../../contexts/authentication";
import { Loading } from "../Loading";
import { Repository as RepositoryComponent } from "../Repository";
import { Repository } from "../../models";
import { TitleSection, SubTitleSection } from "lib-components-desafio-green/components";

export const RepositoriesContainer = () => {
  const { loading, repositories, setStarRepository } = useContext(RepositoryContext);
  const { user } = useContext(AuthContext);

  if (loading)
    return (
      <Loading
        data-test-id="RepositoriesContainerLoading"
        fullScreen={false}
        height={"150px"}
        width={"150px"}
      />
    );

  const handleRepositoryStar = (
    ownerName: string,
    repositoryName: string,
    typeAction: "ADD" | "REMOVE"
  ) => {
    setStarRepository(ownerName, repositoryName, typeAction);
  };

  if (repositories && repositories.length > 0)
    return (
      <>
        <TitleSection> Repositórios </TitleSection>
        {user && (
          <SubTitleSection>
            {" "}
            Olá {user.name}, já que está logado, é possível remover ou adicionar estrelas
            nos repositórios. Clique na estrela para realizar as ações{" "}
          </SubTitleSection>
        )}
        <Styled.RepositoriesContainer data-test-id={"MapContainerRepositories"}>
          {!!repositories &&
            repositories.map((repository: Repository) => {
              return (
                <RepositoryComponent
                  data-test-id={"MapContainerRepository"}
                  key={repository.id}
                  name={repository.name}
                  description={repository.description}
                  url={repository.html_url}
                  isPrivate={repository.private}
                  onClickStar={(typeAction) =>
                    handleRepositoryStar(
                      repository.owner.login,
                      repository.name,
                      typeAction
                    )
                  }
                  isStared={user ? repository.ownerStar : undefined}
                />
              );
            })}
        </Styled.RepositoriesContainer>
      </>
    );

  return null;
};

RepositoriesContainer.displayName = "RepositoriesContainer";
