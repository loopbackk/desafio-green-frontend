import styled from "styled-components";
import tokens from "lib-components-desafio-green/tokens";
import { Card } from "lib-components-desafio-green/components";
import { media } from "../../utils/breakpoints";

export const Base = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: start;
  margin: 10px;
  width: 100%;
  min-width: 100%;
  ${media.forDesktopUp} {
    width: 46%;
    min-width: unset;
  }
`;

export const Title = styled.h2`
  font-weight: bold;
  font-size: 16px;
  margin-left: 10px;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  color: ${tokens.colorNeutralXdark};
`;

export const DescriptionContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  color: ${tokens.colorNeutralDark};
  font-size: 14px;
  word-wrap: break-word;
`;

export const Stars = styled.div`
  margin-left: auto;
`;

export const Description = styled.p`
  margin-top: 2px;
  margin-bottom: 10px;
`;

export const Url = styled.a`
  margin-top: 2px;
  text-decoration: none;
  color: ${tokens.colorBrandPrimaryLight};
`;
