import styled from "styled-components";

export const Base = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const ContainerCompare = styled.div`
  display: flex;
  justify-content: center;
`;
