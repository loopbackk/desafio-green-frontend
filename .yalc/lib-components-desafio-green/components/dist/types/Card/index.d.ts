export declare const Card: {
    ({ children, ...props }: {
        [x: string]: any;
        children?: any;
    }): JSX.Element;
    displayName: string;
};
